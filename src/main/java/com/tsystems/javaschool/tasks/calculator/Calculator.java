package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Calculator {

    private ArrayList<String> expr = new ArrayList<>();
    private int rightBound = 0;
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null) return null;
        statement = statement.replaceAll("\\s+", "");
        char[] statementArray = statement.toCharArray();
        Float result;
        makeArray(statementArray, statement);
        try {
            result = calc(0);
        } catch (Exception t) {
            return null;
        }

        expr.clear();
        result = round(result,4);
        if (result % round(result, 0) == 0) {
            Integer y = Math.round(result);
            return y.toString();
        }
        return result.toString();
    }

    private void makeArray (char[] statementArray, String statement) {
        int lastIndex = 0;
        for(int i = 0; i <= statementArray.length - 1; i++) {
            if ((Character.toString(statementArray[i]).equals("+")) || (Character.toString(statementArray[i]).equals("-")) ||
                (Character.toString(statementArray[i]).equals("/")) || (Character.toString(statementArray[i]).equals("*")) ||
                (Character.toString(statementArray[i]).equals(")")) || (Character.toString(statementArray[i]).equals("("))) {
                expr.add(Character.toString(statementArray[i]));
                lastIndex = lastIndex + 1;
            } else {
                if (i == statementArray.length - 1) {
                    expr.add(statement.substring(lastIndex, i + 1));
                } else {
                    if ((Character.toString(statementArray[i + 1]).equals("+")) || (Character.toString(statementArray[i + 1]).equals("-")) ||
                            (Character.toString(statementArray[i + 1]).equals("/")) || (Character.toString(statementArray[i + 1]).equals("*")) ||
                            (Character.toString(statementArray[i + 1]).equals(")")) || (Character.toString(statementArray[i + 1]).equals("("))) {
                        expr.add(statement.substring(lastIndex, i + 1));
                        lastIndex = lastIndex + statement.substring(lastIndex, i).length() + 1;
                    }
                }
            }
        }
    }

    private Float calc(int position) throws Exception {
        ArrayList<String> simpleExpr = new ArrayList<>();
        float result;

        for (int i = position; i <= expr.size() - 1; i++) {
            if (expr.get(i).equals(")")) {
                rightBound = i;
                break;
            }
            if (expr.get(i).equals("(")) {
                simpleExpr.add(calc(i + 1).toString());
                i = i + (rightBound - i);
            } else {
                simpleExpr.add(expr.get(i));
            }
        }
        result = calcSimpleExpr(simpleExpr);
        return result;
    }

    private Float calcSimpleExpr (ArrayList<String> simpleExpr) throws Exception {
        float result;
        ArrayList<Float> multiply = new ArrayList<>();
        boolean check = false;
        ArrayList<String> simpleExprCalc = new ArrayList<>();
        for (int j = 0; j <= simpleExpr.size() - 2; j++) {

            if ((simpleExpr.get(j).equals("*")) || (simpleExpr.get(j).equals("/"))) {
                if (simpleExpr.get(j).equals("/")) {
                    if (!check) multiply.add(Float.valueOf(simpleExpr.get(j - 1)));
                    if (Float.valueOf(simpleExpr.get(j + 1)) == 0) throw new Exception();
                    multiply.add(1/Float.valueOf(simpleExpr.get(j + 1)));
                } else {
                    if (!check) multiply.add(Float.valueOf(simpleExpr.get(j - 1)));
                    multiply.add(Float.valueOf(simpleExpr.get(j + 1)));
                }
                check = true;
            }

            if ((simpleExpr.get(j).equals("+")) || (simpleExpr.get(j).equals("-"))) {
                if (check) {
                    simpleExprCalc.add(multiply(multiply).toString());
                    check = false;
                    multiply.clear();
                }

                if (j - 2 > 0) {
                    if ((!simpleExpr.get(j - 2).equals("*")) && (!simpleExpr.get(j - 2).equals("/"))) {
                        simpleExprCalc.add(simpleExpr.get(j - 1));
                    }
                } else {
                    simpleExprCalc.add(simpleExpr.get(j - 1));
                }

                simpleExprCalc.add(simpleExpr.get(j));

                if (j == simpleExpr.size() - 2) {
                    simpleExprCalc.add(simpleExpr.get(j + 1));
                }
            }
        }

        if (check) {
            simpleExprCalc.add(multiply(multiply).toString());
        }
        result = Float.valueOf(simpleExprCalc.get(0));

        for (int j = 0; j <= simpleExprCalc.size() - 2; j++) {
            if (simpleExprCalc.get(j).equals("+")) {
                result = result  + Float.valueOf(simpleExprCalc.get(j + 1));
            }
            if (simpleExprCalc.get(j).equals("-")) {
                result = result - Float.valueOf(simpleExprCalc.get(j + 1));
            }
        }

        return result;
    }

    private Float multiply(ArrayList<Float> multiply) {
        float result = 1;
        for (int i = 0; i <= multiply.size() - 1; i++) {
            result = result * multiply.get(i);
        }
        return result;
    }

    private static float round(float value, int places) {
        BigDecimal bd = new BigDecimal(Float.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.floatValue();
    }
}