package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    private int x = 0;
    private int y = 0;
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        check(inputNumbers);
        try {
        Collections.sort(inputNumbers);
        } catch (NullPointerException | OutOfMemoryError t) {
            throw new CannotBuildPyramidException();
        }
        return build(inputNumbers);
    }

    private void check (List<Integer> input) throws CannotBuildPyramidException{
        int i = 0;
        int j = 1;

        while (i < input.size()) {
            i = i + j;
            j++;
        }
        if (i != input.size()) {
            throw new CannotBuildPyramidException();
        }
        y = 1 + 2 * (j - 2);
        x = j - 1;
    }

    private int[][] build (List<Integer> inputNumbers) {
        int index = 0;
        int start = 0;
        int[][] pyramid = new int[x][y];
        for (int i = 0; i <= x - 1; i++) {
            for (int j = 0; j <= y - 1; j++) {
                pyramid[i][j] = 0;
            }
        }
        for (int i = 0; i <= x - 1; i++) {
            if (i % 2 == 0) {
                start = (y + 1) / 2 - i - 1;
            }
            if (i % 2 == 1) {
                start = (y + 1) / 2 - i - 1;
            }
            for (int j = 1; j <= i + 1; j++) {
                pyramid[i][start + (j - 1) * 2] = inputNumbers.get(index);
                index++;
            }
        }
        for (int i = 0; i <= x - 1; i++) {
            System.out.println();
            for (int j = 0; j <= y - 1; j++) {
                System.out.print(" " + pyramid[i][j] + " ");
            }
        }
        return pyramid;
    }
}
