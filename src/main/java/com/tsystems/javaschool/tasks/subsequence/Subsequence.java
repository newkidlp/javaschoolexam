package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int startIndex = 0;
        // TODO: Implement the logic here
        check(x, y);

        if (y.isEmpty()) {
            return x.isEmpty();
        }

        if (x.isEmpty()) {
            return true;
        }

        for (int i = 0; i < x.size(); i++) {
            while ((!x.get(i).equals(y.get(startIndex))) && (startIndex != y.size() - 1)) {
                startIndex++;
            }
            if (!x.get(i).equals(y.get(startIndex))) {
                return false;
            }
        }
        return true;
    }

    private void check(List x, List y) throws IllegalArgumentException {
        if ((x == null) || (y == null)) {
            throw new IllegalArgumentException();
        }
    }

}
